<?php
// Load configuration files
require_once('../config/config.php');
require_once('../config/database.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Get the request URI
    $request_uri = $_SERVER['REQUEST_URI'];

    // Parse the URI to get the post ID
    $parts = explode('/', $request_uri);
    $id = end($parts);

    // Query to get the post by ID
    $query = "SELECT * FROM blog_posts WHERE id = $id";

    // Execute the query
    $result = mysqli_query($conn, $query);

    // Check if the query was successful
    if ($result) {
        // Check if the post with the given ID exists
        if (mysqli_num_rows($result) === 1) {
            // Fetch the post data
            $post = mysqli_fetch_assoc($result);

            // Query to get the number of likes for this post
            $likes_query = "SELECT COUNT(*) AS num_likes FROM post_votes WHERE post_id = $id AND vote_type = 'like'";
            $likes_result = mysqli_query($conn, $likes_query);
            $num_likes = mysqli_fetch_assoc($likes_result)['num_likes'];

            // Query to get the number of dislikes for this post
            $dislikes_query = "SELECT COUNT(*) AS num_dislikes FROM post_votes WHERE post_id = $id AND vote_type = 'dislike'";
            $dislikes_result = mysqli_query($conn, $dislikes_query);
            $num_dislikes = mysqli_fetch_assoc($dislikes_result)['num_dislikes'];

            // Construct the response data
            $response = array(
                'status' => 'success',
                'data' => array(
                    'id' => $post['id'],
                    'title' => $post['title'],
                    'content' => $post['content'],
                    'author' => $post['author'],
                    'date'   => date("l jS \of F Y", strtotime($post['publish_date'])),
                    'likes' => $post['likes'],
                    'dislikes' => $post['dislikes']
                )
            );

            // Set the response header as JSON
            header('Content-Type: application/json');

            // Return the response JSON
            echo json_encode($response);
        } else {
            // Post with the given ID not found
            $response = array(
                'status' => 'error',
                'message' => 'Post not found'
            );

            // Set the response header as JSON
            header('Content-Type: application/json');

            // Return the response JSON
            echo json_encode($response);
        }
    } else {
        // Error executing the query
        $response = array(
            'status' => 'error',
            'message' => 'Error executing query: ' . mysqli_error($conn)
        );

        // Set the response header as JSON
        header('Content-Type: application/json');

        // Return the response JSON
        echo json_encode($response);
    }
}

// Check if IP address has already liked the post
function check_like($conn, $post_id, $ip_address)
{
    $query = "SELECT * FROM post_votes WHERE post_id=? AND user_ip=?";
    $stmt = mysqli_prepare($conn, $query);
    if (!$stmt) {
        die('Error: ' . mysqli_error($conn));
    }
    mysqli_stmt_bind_param($stmt, "is", $post_id, $ip_address);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    return mysqli_num_rows($result) > 0;
}

// Insert like into database
function insert_like($conn, $post_id, $ip_address)
{
    $query = "INSERT INTO post_votes (post_id, user_ip) VALUES (?, ?)";
    $stmt = mysqli_prepare($conn, $query);
    if ($stmt === false) {
        // Handle error, e.g. log it or show a user-friendly message
        return false;
    }
    mysqli_stmt_bind_param($stmt, "is", $post_id, $ip_address);
    mysqli_stmt_execute($stmt);
    return mysqli_stmt_affected_rows($stmt) > 0;
}

// Decrease like count
function decrease_like($conn, $post_id, $ip_address)
{
    // Remove existing like from post_votes table
    $query = "DELETE FROM post_votes WHERE post_id=? AND user_ip=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "is", $post_id, $ip_address);
    mysqli_stmt_execute($stmt);

    // Decrement post's like count in posts table
    $query = "UPDATE blog_posts SET likes=likes-1 WHERE id=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $post_id);
    mysqli_stmt_execute($stmt);
    return mysqli_stmt_affected_rows($stmt) > 0;
}

// Increase like count
function increase_like($conn, $post_id)
{
    $query = "UPDATE blog_posts SET likes=likes+1 WHERE id=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $post_id);
    mysqli_stmt_execute($stmt);
    return mysqli_stmt_affected_rows($stmt) > 0;
}
// Check if IP address has already disliked the post
function check_dislike($conn, $post_id, $ip_address)
{
    $query = "SELECT * FROM post_votes WHERE post_id=? AND user_ip=? AND vote_type='dislike'";
    $stmt = mysqli_prepare($conn, $query);
    if (!$stmt) {
        die('Error: ' . mysqli_error($conn));
    }
    mysqli_stmt_bind_param($stmt, "is", $post_id, $ip_address);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    return mysqli_num_rows($result) > 0;
}

// Insert dislike into database
function insert_dislike($conn, $post_id, $ip_address)
{
    $query = "INSERT INTO post_votes (post_id, user_ip, vote_type) VALUES (?, ?, 'dislike')";
    $stmt = mysqli_prepare($conn, $query);
    if ($stmt === false) {
        // Handle error, e.g. log it or show a user-friendly message
        return false;
    }
    mysqli_stmt_bind_param($stmt, "is", $post_id, $ip_address);
    mysqli_stmt_execute($stmt);
    return mysqli_stmt_affected_rows($stmt) > 0;
}

// Decrease dislike count
function decrease_dislike($conn, $post_id, $ip_address)
{
    // Remove existing dislike from post_votes table
    $query = "DELETE FROM post_votes WHERE post_id=? AND user_ip=? AND vote_type='dislike'";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "is", $post_id, $ip_address);
    mysqli_stmt_execute($stmt);

    // Decrement post's dislike count in posts table
    $query = "UPDATE blog_posts SET dislikes=dislikes-1 WHERE id=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $post_id);
    mysqli_stmt_execute($stmt);
    return mysqli_stmt_affected_rows($stmt) > 0;
}

// Increase dislike count
function increase_dislike($conn, $post_id)
{
    $query = "UPDATE blog_posts SET dislikes=dislikes+1 WHERE id=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $post_id);
    mysqli_stmt_execute($stmt);
    return mysqli_stmt_affected_rows($stmt) > 0;
}

// Handle POST request for liking a post
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // Get the request URI
    $request_uri = $_SERVER['REQUEST_URI'];

    // Split the URI into segments
    $segments = explode('/', $request_uri);

    // Get the ID and IP address from the segments
    $post_id = $segments[6];
    $action = $segments[7];
    $ip_address = $segments[8];

    if ($action == 'like') {
        // Check if user has already liked the post
        if (check_like($conn, $post_id, $ip_address)) {
            // User has already liked the post, decrease like count
            if (decrease_like($conn, $post_id, $ip_address)) {
                // Successfully decreased like count
                http_response_code(200);
                echo json_encode(array('message' => 'Like removed'));
            } else {
                // Failed to decrease like count
                http_response_code(500);
                echo json_encode(array('message' => 'Failed to remove like'));
            }
        } else {
            // User has not yet liked the post, insert like into database
            if (insert_like($conn, $post_id, $ip_address)) {
                // Successfully inserted like
                if (increase_like($conn, $post_id)) {
                    // Query to get the number of likes for this post
                    $likes_query = "SELECT likes FROM blog_posts WHERE id = $post_id";
                    $likes_result = mysqli_query($conn, $likes_query);
                    $num_likes = mysqli_fetch_assoc($likes_result)['likes'];

                    $response = array(
                        'data' => $num_likes
                    );

                    // Successfully increased like count
                    http_response_code(200);
                    echo json_encode($response);
                } else {
                    // Failed to increase like count
                    http_response_code(500);
                    echo json_encode(array('message' => 'Failed to add like'));
                }
            } else {
                // Failed to insert like
                http_response_code(500);
                echo json_encode(array('message' => 'Failed to add like'));
            }
        }
    } else if ($action == 'dislike'){
        // Check if user has already disliked the post
        if (check_dislike($conn, $post_id, $ip_address)) {
            // User has already disliked the post, decrease dislike count
            if (decrease_dislike($conn, $post_id, $ip_address)) {
                // Successfully decreased dislike count
                http_response_code(200);
                echo json_encode(array('message' => 'Dislike removed'));
            } else {
                // Failed to decrease dislike count
                http_response_code(500);
                echo json_encode(array('message' => 'Failed to remove dislike'));
            }
        } else {
            // User has not yet disliked the post, insert dislike into database
            if (insert_dislike($conn, $post_id, $ip_address)) {
                // Successfully inserted dislike
                if (decrease_like($conn, $post_id)) {
                    // Query to get the number of dislikes for this post
                    $dislikes_query = "SELECT dislikes FROM blog_posts WHERE id = $post_id";
                    $dislikes_result = mysqli_query($conn, $dislikes_query);
                    $num_dislikes = mysqli_fetch_assoc($dislikes_result)['dislikes'];

                    $response = array(
                        'data' => $num_dislikes
                    );

                    // Successfully decreased like count and inserted dislike
                    http_response_code(200);
                    echo json_encode($response);
                } else {
                    // Failed to decrease like count
                    http_response_code(500);
                    echo json_encode(array('message' => 'Failed to remove like'));
                }
            } else {
                // Failed to insert dislike
                http_response_code(500);
                echo json_encode(array('message' => 'Failed to add dislike'));
            }
        }
    }

}