import React, { useState } from "react";
import PropTypes from "prop-types";
import styles from "./assets/css/game.module.css";

const Game = ({ choices }) => {
    const [playerChoice, setPlayerChoice] = useState(null);
    const [computerChoice, setComputerChoice] = useState(null);
    const [result, setResult] = useState(null);

    const handleChoice = (choiceIndex) => {
        const computerIndex = Math.floor(Math.random() * choices.length);
        const player = choices[choiceIndex];
        const computer = choices[computerIndex];
        setPlayerChoice(player);
        setComputerChoice(computer);
        if (player === computer) {
            setResult("tie");
        } else if (
            (player === "rock" && computer === "scissors") ||
            (player === "paper" && computer === "rock") ||
            (player === "scissors" && computer === "paper")
        ) {
            setResult("win");
        } else {
            setResult("lose");
        }
    };

    return (
        <div className={styles.container}>
            <h1>Rock Paper Scissors</h1>
            <div className={styles.choices}>
                {choices && choices.map((choice, index) => (
                    <button key={index} onClick={() => handleChoice(index)}>
                        {choice}
                    </button>
                ))}
            </div>
            {playerChoice && computerChoice && (
                <div className={styles.result}>
                    <p>You chose {playerChoice}</p>
                    <p>The computer chose {computerChoice}</p>
                    <h2>{result === "tie" ? "It's a tie!" : `You ${result}!`}</h2>
                </div>
            )}
        </div>
    );
};

Game.propTypes = {
    choices: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Game;