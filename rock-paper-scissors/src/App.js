import React from "react";
import Game from "./game";

const choices = ["rock", "paper", "scissors"];

function App() {
    return (
        <div>
            <Game choices={choices} />
        </div>
    );
}

export default App;
